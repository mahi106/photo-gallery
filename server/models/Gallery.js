const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let Gallery = new Schema({
    url: { type: Object, required: true },
    title: { type: String, required: true },
    location: { type: String },
    tag: { type: [String] }
}, {
    collection: 'gallery'
});

module.exports = mongoose.model('Gallery', Gallery);

