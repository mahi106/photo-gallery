const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let Tags = new Schema({
    post: { type: String, required: true }
}, {
    collection: 'tags'
});

module.exports = mongoose.model('Tags', Tags);