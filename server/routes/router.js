const express = require('express');
const app = express();
const Gallery = require('../models/Gallery');
const Tags = require('../models/Tags');
const multer = require('multer');

app.get('/', (req, res, next) => {
    Gallery.find({})
        .then((data) => {
            res.json(data);
            console.log(req.body);
        })
        .catch(() => {
            res.status(400).send(next);
        });
});

const storage = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, "uploads");
    },
    filename: function(req, file, cb) {
        cb(null, file.originalname);
    }
});

const upload = multer({
    storage: storage,
    limits: {
        fileSize: 1024 * 1024 * 5
    }
}).single("image");

app.post("/add", (req, res) => {
    console.log("fileBB", req.file);
    console.log("bodyBB", req.body);
    upload(req, res, function(err) {
        if (err) {
            console.log("file", req.file);
            console.log("body", req.body);
            res.send(err);
        } else {
            console.log("file", req.file);
            console.log("body", req.body);

            let newImage = new Gallery({
                url: req.file,
                title: req.body.title,
                location: req.body.location,
                tag: req.body.tag.split(',') 
            });
            newImage.save();
            res.send("success");
            // An unknown error occurred when uploading.
        }
        // Everything went fine.
    });
});

app.get('/getTag', (req, res, next) => {
    Tags.find({})
        .then((data) => {
            res.json(data);
            console.log(req.body);
        })
        .catch(() => {
            res.satus(400).send(next);
        });
})

app.post('/addTag', (req, res, next) => {
    let tag = req.body;

    let tags = new Tags(tag);
    tags.save()
        .then(() => {
            res.json('Tag Added');
            console.log('Tag-add');
        })
        .catch(() => {
            res.status(400).send(next);
        });       
});

app.post('/delete/:id', (req, res, next) => {
    Gallery.findByIdAndRemove({ _id: req.params.id }, (err, item) => {
        if(err) res.json(next);
        res.json('Successfully Removed');
    })
})

module.exports = app;
