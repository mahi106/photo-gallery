const express = require('express');
const app = express();
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const cors = require('cors');

let port = process.env.PORT || 5000;

mongoose.Promise = require('bluebird');
mongoose.connect('mongodb://localhost:27017/gallery', { useNewUrlParser: true })
    .then(() => {
        console.log('Connected');
    })
    .catch(err => {
        console.log(err);
    });

const galleryRouter = require('./routes/router');
const tagRouter = require('./routes/router');

app.use(express.static('public'));
app.use(cors());
app.use(bodyParser.urlencoded({ extended: true}));
app.use(bodyParser.json());
app.use('/uploads', express.static('./uploads'));

app.use('/gallery', galleryRouter);
app.use('/tags', tagRouter);

app.listen(port, () => console.log(`Server is up into port ${port}`));