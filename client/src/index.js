import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './components/App';
import AddImage from './components/AddImage';

import { BrowserRouter, Switch, Route } from 'react-router-dom';

import * as serviceWorker from './serviceWorker';

ReactDOM.render(
    <BrowserRouter>
        <Switch>
            <Route exact path="/" component={App} />
            <Route path="/addImage" component={AddImage} />
        </Switch>
    </BrowserRouter>
    , 
    document.getElementById('root')
);

serviceWorker.unregister();
