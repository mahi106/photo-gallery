import React, { Component } from 'react';
import axios from 'axios';
import ProductShow from './ProductShow';

class IndexProduct extends Component {
    state = {
        gallery: []
    }
    onDelete=(oid)=> {
        console.log(oid);
        const updatedPost = this.state.gallery.filter(elem=> elem._id !== oid);
        this.setState({
            gallery: updatedPost
        });
    }
    componentDidMount() {
        let that=this;
        axios.get('http://localhost:5000/gallery')
            .then(response => {
                that.setState({ gallery: response.data });
            }).catch(err => {
                console.log(err);
            });
    }
    render() {
        let product=this.state.gallery.map((obj, i) => (<ProductShow obj={obj} key={i} onclickDelete={this.onDelete}/>));
        return (
            <div style={{ display: 'flex', flexWrap: 'wrap', justifyContent: 'center'}}>
                {product}
            </div>
        );
    }
}

export default IndexProduct;