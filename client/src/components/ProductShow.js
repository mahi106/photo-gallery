import React, { Component } from 'react';
import axios from 'axios';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Modal from '@material-ui/core/Modal';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

function rand() {
    return Math.round(Math.random() * 20) - 10;
}
  
function getModalStyle() {
    const top = 50 + rand();
    const left = 50 + rand();
  
    return {
      top: `${top}%`,
      left: `${left}%`,
      transform: `translate(-${top}%, -${left}%)`,
    };
}
  

const styles = theme => ({
    paper: {
      position: 'absolute',
      width: theme.spacing.unit * 50,
      backgroundColor: theme.palette.background.paper,
      boxShadow: theme.shadows[5],
      padding: theme.spacing.unit * 4,
    },
  });

class ProductShow extends Component {
    constructor(props) {
        super(props);

        this.deleteItem = this.deleteItem.bind(this);
        this.handleOpen = this.handleOpen.bind(this);
        this.handleClose = this.handleClose.bind(this);

        this.state = {
            open: false,
            show: false
        }
    }
    handleOpen = () => {
        this.setState({ open: true });
    };
    handleClose = () => {
        this.setState({ open: false, show: false });
    };
    deleteItem = () => {
        console.log(this.props.obj._id);
        axios.post('http://localhost:5000/gallery/delete/'+ this.props.obj._id)
            .then(() => {
                console.log('Deleted');
                this.props.onclickDelete(this.props.obj._id);
            }).catch(err => {
                console.log(err);
            })
    }
    render() {
        const { classes } = this.props;
        const url = 'http://localhost:5000/uploads/' + this.props.obj.url.filename;
        return (
            <div style={{ margin: 10 }}>
                <Card>
                    <CardActionArea>
                        <CardMedia 
                            style={{ height: 140, width: 150 }}
                            image={url}
                            title={this.props.obj.title}
                        />
                        <CardContent>
                            <Typography gutterBottom variant="h5" component="h2">{this.props.obj.title}</Typography>
                        </CardContent>
                    </CardActionArea>
                    <CardActions>
                        <Button size="small" color="primary" onClick={this.handleOpen}>Details</Button>
                        <Button size="small" color="primary" onClick={this.deleteItem}>Delete</Button>
                        <Modal
                            aria-labelledby="simple-modal-title"
                            aria-describedby="simple-modal-description"
                            open={this.state.open}
                            onClose={this.handleClose}
                        >
                            <div style={getModalStyle()} className={classes.paper}>
                                <Typography gutterBottom variant="title" component="h2">{this.props.obj.title}</Typography>
                                <Typography variant="subheading">{this.props.obj.location}</Typography>
                                <Typography>Tags:</Typography>
                                <div>
                                    {
                                        this.props.obj.tag.map((tag) => <Typography variant="caption">{tag}</Typography>)
                                    }
                                </div>
                            </div>
                        </Modal>
                    </CardActions>
                </Card>
            </div>
        );
    }
}

ProductShow.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ProductShow);