import React, { Component } from 'react';
import DenseAppBar from './DenseAppBar';
import FloatingButton from './FloatingButton';
import IndexProduct from './IndexProduct';
import { Link } from 'react-router-dom';

class Home extends Component {
    render() {
        const title = 'Gallery';
        return (
            <React.Fragment>
                <DenseAppBar title={title} />
                <br />
                <div style={{ display: 'flex', flexDirection: 'column', justifyContent: 'center'}}>
                <div className="add-button">
                    <Link to="/addImage" style={{ textDecoration: 'none'}}><FloatingButton  /></Link>
                </div>
                <IndexProduct />
                </div>
            </React.Fragment>
        );
    }
}

export default Home;