import React, { Component } from 'react';
import '../App.css';

import Home from './Home';

class App extends Component {
  render() {
    return (
      <React.Fragment>
          <div className="App">
            <Home />
          </div>
      </React.Fragment>
    );
  }
}

export default App;
