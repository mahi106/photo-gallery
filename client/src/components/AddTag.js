import React, { Component } from 'react';
import Input from '@material-ui/core/Input';
import Service from './Service';

class AddTag extends Component {
    constructor(props) {
        super(props);

        this.tagService = new Service();
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

        this.state = {
            post: ''
        }
    }
    handleChange = (event) => {
        this.setState({ post: event.target.value })
    }
    handleSubmit = (event) => {
        event.preventDefault();
        this.tagService.addTag(this.state.post);
        console.log(this.state);
        this.setState({ post: ''});
    }
    render() {
        return (
            <React.Fragment>
                <h4>Add Tag:</h4>
                <form onSubmit={this.handleSubmit}>
                    <Input 
                        onChange={this.handleChange}
                        placeholder="Add Tag"
                        value={this.state.post}
                    />
                </form>
            </React.Fragment>
        );
    }
}

export default AddTag;