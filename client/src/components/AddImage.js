import React, { Component } from 'react';
import DenseAppBar from './DenseAppBar';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import Chip from '@material-ui/core/Chip';
import FormControl from '@material-ui/core/FormControl';
import AddTag from './AddTag';

import axios from 'axios';

const styles = theme => ({
    root: {
      display: 'flex',
      flexWrap: 'wrap',
    },
    formControl: {
      margin: theme.spacing.unit,
      minWidth: 120,
      maxWidth: 300,
    },
    chips: {
      display: 'flex',
      flexWrap: 'wrap',
    },
    chip: {
      margin: theme.spacing.unit / 4,
    },
  });

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250,
    },
  },
};

class AddImage extends Component {
    constructor(props) {
        super(props);

        this.urlForm = this.urlForm.bind(this);
        this.titleForm = this.titleForm.bind(this);
        this.locationForm = this.locationForm.bind(this);
        this.tagForm = this.tagForm.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

        this.state = {
            url: {},
            title: '',
            location: '',
            tags: [],
            tag: [],
        }
    }
    urlForm = (event) => {
        this.setState({ url: event.target.files[0] });
    }
    titleForm = (event) => {
        this.setState({ title: event.target.value });
    }
    locationForm = (event) => {
        this.setState({ location: event.target.value })
    }
    tagForm = (event) => {
        this.setState({ tag: event.target.value })
    }
    handleSubmit(event) {
        event.preventDefault();
        // console.log(this.state);
        let formData = new FormData();
        formData.append('image', this.state.url);
        formData.append('title', this.state.title);
        formData.append('location', this.state.location);
        formData.append('tag', this.state.tag);
        console.log(formData);
        axios({
            method: 'post',
            url: 'http://localhost:5000/gallery/add',
            data: formData
        }).then(res => {
                console.log(res);
                this.setState({
                    title: '',
                    location: '',
                    tag: []
                })
            }).catch(err => {
                console.log(err);
            })
    }
    componentDidMount() {
        axios.get('http://localhost:5000/tags/getTag')
        .then(response => {
            this.setState({ tags: response.data })
            console.log(response.data);
        })
        .catch(err => {
            console.log(err);
        }); 
    }
    render() {
        const title='Add Image';
        const { classes, theme } = this.props;
        return (
            <React.Fragment>
                <DenseAppBar title={title} />
                <br />
                <div style={{display: 'flex', flexDirection: 'column', width: '25%', marginLeft: '33%'}}>
                    <h3>Add Image</h3>
                    <form encType="multipart/form-data" onSubmit={this.handleSubmit} style={{ display: 'flex', flexDirection: 'column', justifyContent: 'space-between' }}>
                        <input 
                            accept="image/*"
                            type="file"
                            onChange={this.urlForm}
                            name="image"
                        />
                        <br />
                        <Input 
                            onChange={this.titleForm}
                            placeholder="Title"
                            value={this.state.title}
                            name="title"
                            type="text"
                        />
                        <Input 
                            onChange={this.locationForm}
                            placeholder="Location"
                            value={this.state.location}
                            name="location"
                            type="text"
                        />
                        <FormControl className={classes.formControl}>
                            <InputLabel htmlFor="select-multiple-chip">Tags</InputLabel>
                            <Select 
                                multiple
                                onChange={this.tagForm}
                                input={<Input id="select-multiple-chip" name="tag" value={this.state.tag} />}
                                renderValue={selected => (
                                    <div className={classes.chip}>
                                        {
                                            selected.map(value => (
                                                <Chip key={value} label={value} />
                                            ))
                                        }
                                    </div>
                                )}
                                MenuProps={MenuProps}
                            >
                            {
                                this.state.tags.map(tag => (
                                    <MenuItem
                                        key={tag}
                                        value={tag.post}
                                        style={{
                                            fontWeight:
                                            this.state.tag.indexOf(tag.post) === -1
                                            ? theme.typography.fontWeightRegular
                                            : theme.typography.fontWeightMedium,
                                        }}
                                    >
                                        {tag.post}
                                    </MenuItem>
                                ))
                            }
                            </Select>
                        </FormControl>
                        <Input
                            type="submit"
                            value="Submit"
                            style={{ width: '20%', marginLeft: '33%'}}
                        />
                    </form>
                    <br />
                    <AddTag />
                </div>
            </React.Fragment>
        );
    }
}

AddImage.propTypes = {
    classes: PropTypes.object.isRequired,
    theme: PropTypes.object.isRequired,
};

export default withStyles(styles, { withTheme: true })(AddImage);