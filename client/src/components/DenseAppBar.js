import React, { Component } from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';

class DenseAppBar extends Component {
    render() {
        return (
            <React.Fragment>
                <div style={{ flexGrow: 1 }}>
                    <AppBar position="static">
                        <Toolbar variant="dense">
                            <IconButton style={{ marginLeft: -18, marginRight: 10 }} color="inherit" aria-label="Menu">
                                <MenuIcon />
                            </IconButton>
                            <Typography variant="title" color="inherit">{this.props.title}</Typography>
                        </Toolbar>
                    </AppBar>
                </div>
            </React.Fragment>
        );
    }
}
  
export default DenseAppBar;
  