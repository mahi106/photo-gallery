import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import NavigationIcon from '@material-ui/icons/Navigation';

class FloatingButton extends Component {
    render() {
        return (
            <React.Fragment>
                <Button variant="extendedFab" aria-label="Delete">
                    <NavigationIcon />
                    Add an Image
                </Button>
            </React.Fragment>
        );
    }
}

export default FloatingButton;